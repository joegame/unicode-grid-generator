import jimp from 'jimp';

const INPUT = 'star.jpg'
const DICE = [
  '\u2680',
  '\u2681',
  '\u2682',
  '\u2683',
  '\u2684',
  '\u2685'
]

async function main() {
  // Read the image.
  const image = await jimp.read(INPUT)

  // Resize the image to width 150 and auto height.
  await image.resize(16, 8)
  // await image.color([{ apply: 'greyscale', params: [] }])


  const vals = await gray(image)
  // console.log(vals)
  const string = getDice(vals,16,6)
  console.log(string)

  await image.writeAsync('out_'+INPUT+'.png')
}

function gray(image) {
  return new Promise((res, rej) => {
    let vals = []
    image.scan(0, 0, image.bitmap.width, image.bitmap.height, function(x, y, idx) {
      // x, y is the position of this pixel on the image
      // idx is the position start position of this rgba tuple in the bitmap Buffer
      // this is the image

      let red = this.bitmap.data[idx + 0]
      let green = this.bitmap.data[idx + 1]
      let blue = this.bitmap.data[idx + 2]
      // let alpha = this.bitmap.data[idx + 3];
      let gray = red * 0.2126 + green * 0.7152 + blue * 0.0722;
      this.bitmap.data[idx + 0] = gray
      this.bitmap.data[idx + 1] = gray
      this.bitmap.data[idx + 2] = gray

      vals.push(gray)

      // console.log(`x is ${x}, y is ${y}, rgb is ${red} ${green} ${blue} ${alpha}`)

      if (x == image.bitmap.width - 1 && y == image.bitmap.height - 1) {
        // image scan finished, do your stuff
        res(vals)
      }
    })
  })
}

function getDice(vals, width, n) {
  console.log('get dice function')
  let out = ''
  for (let i = 0; i < vals.length; i++) {
    let v = Math.floor(vals[i] / (255/n))
    out += DICE[v]
    if(i%width === width-1){
      out += '\n'
    }
    // console.log(vals[i])
  }
  return out
}

main();
